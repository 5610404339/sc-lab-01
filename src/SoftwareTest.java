
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;



	

	public class SoftwareTest {

		class ListenerMgr implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);

			}

		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			new SoftwareTest();
		}

		public SoftwareTest() {
			frame = new SoftwareFrame();
			frame.pack();
			frame.setVisible(true);
			frame.setSize(800, 200);
			list = new ListenerMgr();
			frame.setListener(list);
			setTestCase();
		}

		public void setTestCase() {
			/*
			 * Student may modify this code and write your result here
			 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
			 */
			Card c1 = new Card();
			frame.setResult("Check Balance  "+" " +c1.getBalance());
			c1.deposite(500);
			frame.extendResult("Add money "+" " +c1.getBalance());
			c1.withdraw(200);
			frame.extendResult("Buy some food "+" " + c1.getBalance());
				
			frame.extendResult("Balance  "+c1.getBalance());

		}

		ActionListener list;
		SoftwareFrame frame;
	}


