
public class Card {
     private double money ;
      
     public Card(){
    	 money = 0;
     }
     public Card(double amount){
    	 money = amount ;
     }
     public void deposite (double value){
    	money +=value ;
     }
     public void withdraw(double value){
    	money -=value ;
     }
     
     public double getBalance(){
    	
		return money ;
     }
}